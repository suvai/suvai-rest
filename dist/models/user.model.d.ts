import { Entity } from '@loopback/repository';
export declare class Users extends Entity {
    usersid?: string;
    usertypekey: string;
    firstname?: string;
    lastname?: string;
    phone?: string;
    email?: string;
    password?: string;
    updatedby?: string;
    updatedon?: string;
    insertedby?: string;
    insertedon?: string;
    activeflag?: number;
    [prop: string]: any;
    constructor(data?: Partial<Users>);
}
export interface UserRelations {
}
export declare type UserWithRelations = Users & UserRelations;
