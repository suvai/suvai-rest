import { Count, Filter, Where } from '@loopback/repository';
import { Users } from '../models';
import { UserRepository } from '../repositories';
export declare class UserController {
    userRepository: UserRepository;
    constructor(userRepository: UserRepository);
    create(user: Users): Promise<Users>;
    count(where?: Where<Users>): Promise<Count>;
    find(filter?: Filter<Users>): Promise<Users[]>;
    updateAll(user: Users, where?: Where<Users>): Promise<Count>;
    findById(id: string, filter?: Filter<Users>): Promise<Users>;
    updateById(id: string, user: Users): Promise<void>;
    replaceById(id: string, user: Users): Promise<void>;
    deleteById(id: string): Promise<void>;
}
