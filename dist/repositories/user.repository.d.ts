import { DefaultCrudRepository } from '@loopback/repository';
import { Users, UserRelations } from '../models';
import { SuvaiDataSource } from '../datasources';
export declare class UserRepository extends DefaultCrudRepository<Users, typeof Users.prototype.usersid, UserRelations> {
    constructor(dataSource: SuvaiDataSource);
}
