import { DefaultCrudRepository } from '@loopback/repository';
import { Users, UserRelations } from '../models';
import { SuvaiDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class UserRepository extends DefaultCrudRepository<
  Users,
  typeof Users.prototype.usersid,
  UserRelations
  > {
  constructor(
    @inject('datasources.suvai') dataSource: SuvaiDataSource,
  ) {
    super(Users, dataSource);
  }
}
