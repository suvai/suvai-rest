import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: true } })
export class Users extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  usersid?: string;

  @property({
    type: 'string',
    required: true,
  })
  usertypekey: string;

  @property({
    type: 'string',
    required: true,
  })
  firstname?: string;

  @property({
    type: 'string',
    required: true,
  })
  lastname?: string;

  @property({
    type: 'string',
  })
  phone?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'string',
    required: true,
  })
  updatedby?: string;

  @property({
    type: 'date',
    required: true,
  })
  updatedon?: string;

  @property({
    type: 'string',
    required: true,
  })
  insertedby?: string;

  @property({
    type: 'date',
    required: true,
  })
  insertedon?: string;

  @property({
    type: 'number',
  })
  activeflag?: number;

  // @property({
  //   type: 'string'
  // })
  // additionalProp1?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Users>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = Users & UserRelations;
